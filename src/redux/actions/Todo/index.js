import { setLoading } from 'store/actions';
import {
   requestGet,
   requestPost,
   requestPut,
   requestDelete,
} from 'config/engine';
import { showError, showSuccess } from 'utils/showMessage';
import { TODOS } from 'initHttp';
import { LIST_TODO, CREATE_TODO, DELETE_TODO } from 'initType';

export const getListTodo = token => {
   return async dispatch => {
      dispatch(setLoading(true));
      try {
         let res = await requestGet(TODOS, null, token);

         dispatch({ type: LIST_TODO, payload: res.todo });
      } catch (error) {
         showError(error?.response?.data?.message);
      }
      dispatch(setLoading(false));
   };
};

export const createTodo = (data, token) => {
   return async dispatch => {
      dispatch(setLoading(true));
      try {
         await requestPost(TODOS, data, token);
         dispatch({ type: CREATE_TODO, payload: true });
         showSuccess('Todo berhasil ditambahkan.');
      } catch (error) {
         showError(error?.response?.data?.message);
      }
      dispatch(setLoading(false));
   };
};

export const updateTodo = (id, params, token) => {
   return async dispatch => {
      dispatch(setLoading(true));
      try {
         await requestPut(`${TODOS}/${id}`, null, params, token);
      } catch (error) {
         showError(error?.response?.data?.message);
      }
      dispatch(setLoading(false));
   };
};

export const deleteTodo = (id, token) => {
   return async dispatch => {
      dispatch(setLoading(true));
      try {
         await requestDelete(`${TODOS}/${id}`, null, token);
         dispatch({ type: DELETE_TODO, payload: true });
      } catch (error) {
         console.log('error: ', error);
         showError(error?.response?.data?.message);
      }
      dispatch(setLoading(false));
   };
};

export const clearCreateTodo = () => ({
   type: CREATE_TODO,
   payload: false,
});

export const clearDeleteTodo = () => ({
   type: DELETE_TODO,
   payload: false,
});
