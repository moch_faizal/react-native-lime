import { SET_LOADING } from 'initType';

export const setLoading = value => ({
   type: SET_LOADING,
   payload: value,
});
