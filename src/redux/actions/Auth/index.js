import { setLoading } from 'store/actions';
import { requestPost } from 'config/engine';
import { showError, showSuccess } from 'utils/showMessage';
import { AUTH } from 'initHttp';
import { IS_LOGIN, AUTH_TOKEN } from 'initType';
import { storeData, removeData } from 'utils';

export const requestLogin = data => {
   return async dispatch => {
      dispatch(setLoading(true));
      try {
         let res = await requestPost(AUTH, data);

         storeData('user', res.user);
         storeData('accessToken', res.access_token);
         dispatch(setIsLogin(true));
         dispatch(setAuthToken(res.access_token));
         showSuccess('Login berhasil');
      } catch (error) {
         showError(error?.response?.data?.message);
      }
      dispatch(setLoading(false));
   };
};

export const requestLogout = () => {
   return async dispatch => {
      dispatch(setLoading(true));
      dispatch(setIsLogin(false));
      removeData('accessToken');
      showSuccess('Logout berhasil');
      dispatch(setLoading(false));
   };
};

export const setIsLogin = value => ({
   type: IS_LOGIN,
   payload: value,
});

export const setAuthToken = value => ({
   type: AUTH_TOKEN,
   payload: value,
});
