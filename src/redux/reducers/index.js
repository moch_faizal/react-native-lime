import { combineReducers } from 'redux';
import General from './General';
import Auth from './Auth';
import Todo from './Todo';

export default combineReducers({
   General,
   Auth,
   Todo,
});
