import { SET_LOADING } from 'initType';

const initState = {
   loading: false,
};

const General = (state = initState, action) => {
   switch (action.type) {
      case SET_LOADING:
         return {
            ...state,
            loading: action.payload,
         };

      default:
         return state;
   }
};

export default General;
