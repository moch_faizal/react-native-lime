import { IS_LOGIN, AUTH_TOKEN } from 'initType';

const initState = {
   isLogin: false,
   token: false,
};

const Auth = (state = initState, action) => {
   switch (action.type) {
      case IS_LOGIN:
         return {
            ...state,
            isLogin: action.payload,
         };

      case AUTH_TOKEN:
         return {
            ...state,
            token: action.payload,
         };

      default:
         return state;
   }
};

export default Auth;
