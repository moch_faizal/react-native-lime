import { LIST_TODO, CREATE_TODO, DELETE_TODO } from 'initType';

const initState = {
   todos: false,
   create: false,
   update: false,
   delete: false,
};

const Todo = (state = initState, action) => {
   switch (action.type) {
      case LIST_TODO:
         return {
            ...state,
            todos: action.payload,
         };

      case CREATE_TODO:
         return {
            ...state,
            create: action.payload,
         };

      case DELETE_TODO:
         return {
            ...state,
            delete: action.payload,
         };

      default:
         return state;
   }
};

export default Todo;
