// General
export const SET_LOADING = 'SET_LOADING';

// Auth
export const IS_LOGIN = 'IS_LOGIN';
export const AUTH_TOKEN = 'AUTH_TOKEN';

// Todos
export const LIST_TODO = 'LIST_TODO';
export const CREATE_TODO = 'CREATE_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const DELETE_TODO = 'DELETE_TODO';
