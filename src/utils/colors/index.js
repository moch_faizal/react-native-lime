const mainColors = {
   colorPrimary: '#0f2d52',
   colorSecondary: '#8d9eb0',
   colorAccent: '#2673D1',
   colorError: '#af0f0f',
   colorSuccess: '#64bd45',
   colorWarning: '#feb93a',
   colorInfo: '#37b7ef',
   colorTransparentBlack: '#484c4fb3',
};

export const colors = {
   primary: mainColors.colorPrimary,
   secondary: mainColors.colorSecondary,
   accent: mainColors.colorAccent,
   white: 'white',
   black: 'black',
   error: mainColors.colorError,
   success: mainColors.colorSuccess,
   info: mainColors.colorInfo,
   warning: mainColors.colorWarning,
   transparentBlack: mainColors.colorTransparentBlack,
};
