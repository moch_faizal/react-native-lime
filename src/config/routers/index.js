import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Splash, Login, MainApp } from 'pages';

const Router = () => {
   const Stack = createStackNavigator();

   return (
      <Stack.Navigator
         initialRouteName="Splash"
         screenOptions={{
            gestureEnabled: true,
            gestureDirection: 'horizontal',
         }}
         headerMode="float"
         animation="fade">
         <Stack.Screen
            name="Splash"
            component={Splash}
            options={{ headerShown: false, headerStatusBarHeight: 0 }}
         />
         <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
         />
         <Stack.Screen
            name="MainApp"
            component={MainApp}
            options={{ headerShown: false }}
         />
      </Stack.Navigator>
   );
};

export default Router;
