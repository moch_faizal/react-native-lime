import axios from 'axios';
import { objectString } from 'utils';
import { BASE_URL } from 'initHttp';

const headers = token => {
   let data = {
      'Content-Type': 'application/json',
   };

   if (token) {
      data = {
         'Content-Type': 'application/json',
         Authorization: `Bearer ${token}`,
      };
   }

   return data;
};

export const requestPost = (url, params, token) => {
   return new Promise((resolve, reject) => {
      axios
         .post(`${BASE_URL}${url}`, params, {
            headers: headers(token),
         })
         .then(response => {
            resolve(response.data);
         })
         .catch(error => {
            reject(error);
         });
   });
};

export const requestGet = (url, params, token) => {
   return new Promise((resolve, reject) => {
      axios
         .get(`${BASE_URL}${url}${params ? objectString(params) : ''}`, {
            headers: headers(token),
         })
         .then(response => {
            resolve(response.data);
         })
         .then(error => {
            reject(error);
         })
         .catch(error => {
            reject(error);
         });
   });
};

export const requestPut = (url, params, body, token) => {
   return new Promise((resolve, reject) => {
      axios
         .put(`${BASE_URL}${url}${params ? objectString(params) : ''}`, body, {
            headers: headers(token),
         })
         .then(response => {
            resolve(response.data);
         })
         .catch(error => {
            reject(error);
         });
   });
};

export const requestDelete = (url, body, token) => {
   return new Promise((resolve, reject) => {
      axios
         .delete(
            `${BASE_URL}${url}`,
            body
               ? {
                    headers: headers(token),
                    data: body,
                 }
               : {
                    headers: headers(token),
                 },
         )
         .then(response => {
            resolve(response.data);
         })
         .catch(error => {
            reject(error);
         });
   });
};

export const requestAnyGet = (url, params, headers) => {
   return new Promise((resolve, reject) => {
      axios
         .get(
            `${BASE_URL}${url}${objectString(params)}`,
            headers && {
               headers: headers,
            },
         )
         .then(response => {
            resolve(response.data);
         })
         .catch(error => {
            reject(error);
         });
   });
};
