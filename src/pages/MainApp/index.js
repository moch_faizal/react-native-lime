/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import CheckBox from '@react-native-community/checkbox';
import { View, Text, StatusBar, ScrollView, Modal } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'utils/useForm';
import GlobalStyle from 'styles';
import { getData } from 'utils';
import { Gap, Button, Input } from 'components';
import { colors } from 'utils/colors';
import * as Icon from 'react-native-feather';
import {
   createTodo,
   clearCreateTodo,
   getListTodo,
   updateTodo,
   deleteTodo,
   clearDeleteTodo,
} from 'store/actions';
import { showSuccess } from 'utils/showMessage';

function MainApp({ navigation }) {
   const [state, setState] = useState({
      user: false,
      list: false,
   });
   const [form, setForm] = useForm({ name: '' });
   const [errors, setErrors] = useState({});
   const [modalVisible, setModalVisible] = useState(false);
   const [toggleCheckBox, setToggleCheckBox] = useState([]);
   const { Auth, Todo } = useSelector(state => state);
   const dispatch = useDispatch();

   const requestData = () => {
      dispatch(getListTodo(Auth.token));
   };

   useEffect(() => {
      getData('user').then(value => {
         if (value) {
            setState({ ...state, user: value });
         }
      });
      requestData();
   }, []);

   useEffect(() => {
      if (Todo.todos) {
         let newState = {};
         for (let i = 0; i < Todo.todos.length; i++) {
            const element = Todo.todos[i];
            newState[element.id] = element.is_checked;
         }

         setToggleCheckBox(newState);
         setState({ ...state, list: Todo.todos });
      }
   }, [Todo.todos]);

   const actionCheckbox = (newValue, item) => {
      let newState = {};
      for (let i = 0; i < state.list.length; i++) {
         const element = state.list[i];
         if (element.id == item.id) {
            newState[element.id] = newValue;
         } else {
            newState[element.id] = toggleCheckBox[element.id];
         }
      }

      setToggleCheckBox(newState);

      let data = {
         name: item.name,
         is_checked: newValue,
      };

      dispatch(updateTodo(item.id, data, Auth.token));
   };

   const handleValidation = () => {
      let fields = form;
      let errors = {};
      let formIsValid = true;

      if (!fields.name) {
         formIsValid = false;
         errors.email = 'Tidak boleh kosong.';
      }

      setErrors(errors);
      return formIsValid;
   };

   const sendData = () => {
      if (handleValidation()) {
         setModalVisible(!modalVisible);
         let data = {
            name: form.name,
            is_checked: false,
         };

         dispatch(createTodo(data, Auth.token));
      }
   };

   useEffect(() => {
      if (Todo.create) {
         requestData();
         showSuccess('Todo berhasil ditambahkan.');
         setForm('name', '');
         dispatch(clearCreateTodo());
      }
   }, [Todo.create]);

   useEffect(() => {
      if (Todo.delete) {
         requestData();
         showSuccess('Todo berhasil dihapus.');
         dispatch(clearDeleteTodo());
      }
   }, [Todo.delete]);

   return (
      <View style={GlobalStyle.containerColumn}>
         <StatusBar backgroundColor={colors.accent} translucent={false} />
         <ScrollView>
            <View style={styles.background}>
               <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.text.welcome}>
                     Welcome, {state.user.name}
                  </Text>
               </View>
               <Gap height={20} />
               {state.list &&
                  state.list.length > 0 &&
                  state.list.map((item, index) => {
                     return (
                        <View
                           key={index}
                           style={[
                              GlobalStyle.boxWithShadow,
                              {
                                 flexDirection: 'row',
                                 backgroundColor: colors.white,
                                 alignContent: 'space-around',
                                 marginVertical: 5,
                                 borderRadius: 10,
                                 padding: 18,
                              },
                           ]}>
                           <CheckBox
                              hideBox={false}
                              disabled={false}
                              style={{
                                 width: 20,
                                 height: 20,
                                 alignSelf: 'center',
                              }}
                              boxType="square"
                              onTintColor={colors.primary}
                              onCheckColor={colors.primary}
                              tintColors={{ true: colors.primary }}
                              value={toggleCheckBox[item.id]}
                              onValueChange={newValue => {
                                 actionCheckbox(newValue, item);
                              }}
                           />

                           <Text
                              style={{
                                 left: 10,
                                 flex: 1,
                                 alignSelf: 'center',
                              }}>
                              {item.name}
                           </Text>

                           <Icon.Trash2
                              onPress={() => {
                                 dispatch(deleteTodo(item.id, Auth.token));
                              }}
                              style={{
                                 flex: 1,
                                 alignSelf: 'center',
                              }}
                              width={18}
                              height={18}
                              fill="none"
                              stroke={colors.error}
                           />
                        </View>
                     );
                  })}
               <Gap height={20} />
               <Button
                  style={{
                     display: 'flex',
                     flexDirection: 'row',
                     padding: 10,
                     borderRadius: 10,
                     minHeight: 20,
                     backgroundColor: colors.accent,
                  }}
                  title="Tambah Todo"
                  onPress={() => {
                     setModalVisible(true);
                  }}
               />
            </View>
         </ScrollView>

         <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            style={{
               backgroundColor: colors.white,
               borderRadius: 10,
               padding: 30,
               shadowColor: '#000',
               shadowOffset: { width: 0, height: 1 },
               shadowOpacity: 0.25,
               shadowRadius: 3.84,
               elevation: 5,
               display: 'flex',
            }}
            onRequestClose={() => {
               setModalVisible(false);
            }}>
            <View
               style={{
                  flex: 1,
                  backgroundColor: '#484c4fb3',
                  justifyContent: 'center',
               }}>
               <View
                  style={{
                     backgroundColor: colors.white,
                     marginStart: 30,
                     marginEnd: 30,
                     padding: 20,
                     borderRadius: 10,
                  }}>
                  <View
                     style={{
                        flexDirection: 'row',
                        alignContent: 'space-between',
                     }}>
                     <Text style={{ flex: 1, fontSize: 16, fontWeight: '600' }}>
                        Tambah Todo
                     </Text>
                     <Icon.X
                        onPress={() => setModalVisible(!modalVisible)}
                        style={{ flex: 1 }}
                        width={16}
                        height={16}
                        stroke={colors.secondary}
                     />
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                     <Input
                        placeholder={'Masukkan nama todo'}
                        value={form.name}
                        withLabel={false}
                        gap={20}
                        onChangeText={value => setForm('name', value)}
                        error={errors.name}
                        animate={'bounceIn'}
                     />
                     <Gap height={10} />
                     <Button
                        style={{
                           display: 'flex',
                           flexDirection: 'row',
                           padding: 10,
                           borderRadius: 10,
                           minHeight: 20,
                           backgroundColor: colors.accent,
                        }}
                        title="Tambahkan"
                        onPress={sendData}
                     />
                  </View>
               </View>
            </View>
         </Modal>
      </View>
   );
}

const styles = {
   background: { padding: 20, backgroudColor: colors.primary, flex: 1 },
   text: {
      welcome: {
         color: colors.primary,
         fontSize: 20,
         fontWeight: '700',
      },
   },
};

export default MainApp;
