/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { View, Text, Image, ScrollView, StatusBar } from 'react-native';
import { colors } from 'utils/colors';
import { useDispatch, useSelector } from 'react-redux';
import { Gap, Input, Button } from 'components';
import GlobalStyle from 'styles';
import { useForm } from 'utils/useForm';
import { requestLogin } from 'store/actions';
import { getData } from 'utils';

const Login = ({ navigation }) => {
   const [form, setForm] = useForm({
      email: '',
      password: '',
   });
   const [errors, setErrors] = useState({});
   const { Auth } = useSelector(state => state);
   const dispatch = useDispatch();

   const handleValidation = () => {
      let fields = form;
      let errors = {};
      let formIsValid = true;

      if (!fields.email) {
         formIsValid = false;
         errors.email = 'Tidak boleh kosong.';
      } else {
         let reg = /\S+@\S+\.\S+/;
         if (!reg.test(fields.email)) {
            formIsValid = false;
            errors.email = 'Email tidak valid.';
         }
      }

      if (!fields.password) {
         formIsValid = false;
         errors.password = 'Tidak boleh kosong.';
      } else {
         if (fields.password.length < 8) {
            formIsValid = false;
            errors.password = 'Password minimal 8 karakter.';
         }
      }

      setErrors(errors);
      return formIsValid;
   };

   const sendData = () => {
      if (handleValidation()) {
         let dataAuth = {
            email: form.email,
            password: form.password,
         };

         dispatch(requestLogin(dataAuth));
      }
   };

   useEffect(() => {
      if (Auth.isLogin) {
         navigation.replace('MainApp');
      }
   }, [Auth.isLogin]);

   return (
      <View style={GlobalStyle.containerColumn}>
         <StatusBar backgroundColor={'#E5E5E5'} translucent={false} />
         <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.page}>
               <Image
                  source={require('assets/illustrations/main-login.png')}
                  style={styles.images.logo}
               />

               <Gap height={30} />
               <View style={styles.background.quote}>
                  <Text style={styles.text.welcome}>WELCOME BACK</Text>
                  <Gap height={6} />
                  <Text style={styles.text.quote}>Account Log in</Text>
               </View>

               <View>
                  <Input
                     placeholder={'Email'}
                     value={form.email}
                     withLabel={false}
                     gap={20}
                     onChangeText={value => setForm('email', value)}
                     keyboardType={'email-address'}
                     error={errors.email}
                     animate={'bounceIn'}
                  />

                  <Input
                     placeholder={'Password'}
                     value={form.password}
                     withLabel={false}
                     gap={20}
                     onChangeText={value => setForm('password', value)}
                     secureTextEntry={true}
                     error={errors.password}
                     animate={'bounceIn'}
                  />
                  <Gap height={15} />
                  <Button title="Masuk" onPress={sendData} />
               </View>
            </View>
         </ScrollView>
      </View>
   );
};

const styles = {
   page: {
      padding: 25,
   },
   background: {
      quote: {
         flexDirection: 'column',
      },
   },
   text: {
      welcome: {
         color: colors.secondary,
         fontSize: 16,
         fontStyle: 'normal',
      },
      quote: {
         color: colors.primary,
         fontSize: 20,
         fontWeight: '700',
      },
      lupaPassword: {
         alignSelf: 'flex-end',
         color: colors.secondary,
      },
      daftar: {
         marginStart: 5,
         color: colors.secondary,
      },
   },
   images: {
      logo: {
         width: '90%',
         marginTop: 20,
         alignSelf: 'center',
         resizeMode: 'contain',
      },
   },
};

export default Login;
