/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { View, ImageBackground, Image, StatusBar } from 'react-native';
import { colors } from 'utils/colors';
import { useDispatch } from 'react-redux';
import { setIsLogin } from 'store/actions';
import { getData } from 'utils';
import { setAuthToken } from '../../redux/actions/Auth';

const Splash = ({ navigation }) => {
   const dispatch = useDispatch();

   useEffect(() => {
      setTimeout(() => {
         getData('accessToken').then(res => {
            if (!res) {
               navigation.replace('Login');
            } else {
               dispatch(setIsLogin(true));
               dispatch(setAuthToken(res));
               navigation.replace('MainApp');
            }
         });
      }, 3000);
   }, []);

   return (
      <View style={styles.container}>
         <StatusBar backgroundColor={'#00000000'} translucent={true} />
         <ImageBackground
            source={require('assets/img/background-splash.png')}
            style={styles.image.background}>
            <Image
               source={require('assets/img/main-logo.png')}
               style={styles.image.logo}
            />
         </ImageBackground>
      </View>
   );
};

const styles = {
   container: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      backgroundColor: colors.white,
   },
   image: {
      background: {
         display: 'flex',
         flex: 1,
         flexDirection: 'column',
         justifyContent: 'center',
         alignItems: 'center',
         resizeMode: 'cover',
      },
      logo: {
         flex: 1,
         width: 140,
         height: 140,
         resizeMode: 'contain',
      },
   },
};

export default Splash;
