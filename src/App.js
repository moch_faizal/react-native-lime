/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import FlashMessage from 'react-native-flash-message';
import Router from 'config/routers';
import { Provider, useSelector } from 'react-redux';
import { Loading } from 'components';
import store from 'store/store';
import { SafeAreaView, StyleSheet, Platform } from 'react-native';

const MainApp = () => {
   const { General } = useSelector(state => state);

   return (
      <NavigationContainer>
         <SafeAreaView style={styles.container}>
            <Router />
            {Platform.OS === 'android' && <FlashMessage position="top" />}
         </SafeAreaView>
         {Platform.OS === 'ios' && <FlashMessage position="top" />}
         {General.loading && <Loading />}
      </NavigationContainer>
   );
};

const App = () => {
   return (
      <Provider store={store}>
         <MainApp />
      </Provider>
   );
};

export default App;

const styles = StyleSheet.create({
   container: {
      flex: 1,
   },
});
