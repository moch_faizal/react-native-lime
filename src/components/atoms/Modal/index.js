import React from 'react';
import { Modal as CustomModal, Text, View } from 'react-native';
import { colors } from 'utils/colors';
import Button from '../Button';
import ButtonOutline from '../ButtonOutline';
import Gap from '../Gap';

const Modal = ({
   stateVisible,
   text,
   button: { btnConfirm, btnCancel },
   callback,
}) => {
   const handleCallback = action => {
      callback(action);
   };

   return (
      <CustomModal
         animationType="fade"
         transparent={true}
         visible={stateVisible}
         onRequestClose={() => handleCallback(false)}
         style={{
            backgroundColor: colors.white,
            borderRadius: 10,
            padding: 30,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
            display: 'flex',
         }}>
         <View
            style={{
               flex: 1,
               backgroundColor: colors.transparentBlack,
               justifyContent: 'center',
            }}>
            <View
               style={{
                  backgroundColor: colors.white,
                  marginStart: 30,
                  marginEnd: 30,
                  padding: 20,
                  borderRadius: 10,
               }}>
               <Text>{text}</Text>
               <Gap height={15} />
               <View
                  style={{
                     position: 'relative',
                     flexDirection: 'row',
                     paddingBottom: 20,
                  }}>
                  <View style={{ flex: 1 }}>
                     <ButtonOutline
                        title={btnCancel}
                        onPress={() => handleCallback(false)}
                     />
                  </View>
                  <Gap width={10} />
                  <View style={{ flex: 1 }}>
                     <Button
                        onPress={() => handleCallback('yes')}
                        title={btnConfirm}
                     />
                  </View>
               </View>
            </View>
         </View>
      </CustomModal>
   );
};

const styles = {
   modalView: {
      marginVertical: 12,
      backgroundColor: 'white',
      borderRadius: 10,
      paddingVertical: 8,
      paddingHorizontal: 12,
      shadowColor: '#000',
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
   },
};

export default Modal;
