import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { colors } from 'utils/colors';

const Button = ({ onPress, ...props }) => {
   return (
      <TouchableOpacity
         style={props.style ? props.style : styles.container}
         onPress={onPress}
         disabled={props.disabled}>
         <Text style={styles.text}>{props.title}</Text>
      </TouchableOpacity>
   );
};

export default Button;

const styles = StyleSheet.create({
   container: {
      display: 'flex',
      flexDirection: 'row',
      flex: 1,
      padding: 10,
      borderRadius: 10,
      minHeight: 50,
      backgroundColor: colors.accent,
   },
   text: {
      flex: 1,
      alignSelf: 'center',
      textAlign: 'center',
      color: colors.white,
   },
});
