import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { colors } from 'utils/colors';
import Gap from '../Gap';
import ValidateMsg from '../ValidateMsg';

const Input = ({ onChangeText, onBlur, onEndEditing, ...props }) => {
   return (
      <View>
         {props.gap > 0 && <Gap height={props.gap} />}

         {props.withLabel === true && (
            <Text style={styles.text}>{props.placeholder}</Text>
         )}

         <TextInput
            style={!props.style ? styles.default : styles.error}
            placeholder={props.placeholder}
            placeholderTextColor={colors.softGray}
            onChangeText={onChangeText}
            secureTextEntry={props.secureTextEntry}
            keyboardType={props.keyboardType}
            onBlur={onBlur}
            onEndEditing={onEndEditing}
            value={!props.value ? '' : props.value}
         />

         {props.error && (
            <ValidateMsg animation={props.animate} message={props.error} />
         )}
      </View>
   );
};

export default Input;

const styles = StyleSheet.create({
   default: {
      height: 55,
      borderWidth: 1.5,
      color: colors.black,
      borderColor: '#a8c7ed',
      borderRadius: 10,
      paddingTop: 18.5,
      paddingBottom: 18.5,
      paddingStart: 16,
   },
   error: {
      height: 55,
      borderWidth: 1.5,
      color: colors.black,
      borderColor: colors.error,
      borderRadius: 10,
      paddingTop: 18.5,
      paddingBottom: 18.5,
      paddingStart: 16,
   },
   text: {
      fontSize: 14,
      marginBottom: 10,
   },
});
