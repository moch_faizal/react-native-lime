import React from 'react';
import { Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import GlobalStyle from 'src/styles';

const ValidateMsg = ({ ...props }) => {
   return (
      <Animatable.View
         animation={props.animation ? props.animation : 'fadeIn'}
         duration={500}>
         <Text style={GlobalStyle.errorMsg}>* {props.message}</Text>
      </Animatable.View>
   );
};

export default ValidateMsg;
