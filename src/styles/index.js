import { StyleSheet } from 'react-native';
import { colors } from 'utils/colors';

export default StyleSheet.create({
   containerColumn: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
      backgroundColor: '#E5E5E5',
   },
   containerRow: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
   },
   errorMsg: {
      top: 4,
      start: 3,
      color: colors.error,
      fontSize: 10,
   },
   toolbar: top => ({
      flex: 1,
      zIndex: 10,
      position: 'absolute',
      paddingHorizontal: 6,
      paddingVertical: 5,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      top: top ? top : 0,
   }),
   boxWithShadow: {
      shadowOffset: { width: 5, height: 5 },
      shadowColor: colors.black,
      shadowOpacity: 0.1,
   },
});
